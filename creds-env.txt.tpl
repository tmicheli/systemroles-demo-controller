# the Base URI of our automation controller cluster node
export CONTROLLER_HOST=ansible-1.vx72f.sandbox1760.opentlc.com

# the user name
export CONTROLLER_USERNAME=admin

# and the password
export CONTROLLER_PASSWORD='MTMzMjY4'

# do not verify the SSL certificate, in production, you will use proper SSL certificates and not need this option or set it to True
export CONTROLLER_VERIFY_SSL=false
